#
#   Maven stage
#
FROM maven:3.6.3-openjdk-15 AS maven
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package assembly:single
#
#   Build stage
#
FROM openjdk:15
COPY --from=maven /home/app/target/market-1.0-jar-with-dependencies.jar /home/app/target/market.jar
COPY src/csvs /home/app/src/csvs
WORKDIR /home/app/target
EXPOSE 5001
CMD ["java","-jar","market.jar"]