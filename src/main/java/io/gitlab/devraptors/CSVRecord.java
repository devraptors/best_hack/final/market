package io.gitlab.devraptors;

import java.time.LocalTime;

public class CSVRecord {
    private final String ticker;
    private final LocalTime time;
    private final double last;

    public static CSVRecord create(String ticker, LocalTime time, double last) {
        return new CSVRecord(ticker, time, last);
    }

    private CSVRecord(String ticker, LocalTime time, double last) {
        this.ticker = ticker;
        this.time = time;
        this.last = last;
    }

    public LocalTime getTime() {
        return time;
    }

    public double getLast() {
        return last;
    }

    public String getTicker() {
        return ticker;
    }
}
