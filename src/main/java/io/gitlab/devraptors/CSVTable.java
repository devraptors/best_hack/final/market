package io.gitlab.devraptors;

import java.nio.file.Path;
import java.util.List;

public class CSVTable {
    private final List<CSVRecord> records;
    private final Path file;

    public static CSVTable create(List<CSVRecord> records, Path file) {
        return new CSVTable(records, file);
    }

    private CSVTable(List<CSVRecord> records, Path file) {
        this.records = records;
        this.file = file;
    }

    public List<CSVRecord> getRecords() {
        return records;
    }

    public Path getFile() {
        return file;
    }
}
