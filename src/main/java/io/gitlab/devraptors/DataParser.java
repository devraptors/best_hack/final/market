package io.gitlab.devraptors;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DataParser {

    private static final Pattern pattern = Pattern.compile("([^;]*);[^;]*;[^;]*;([^;]*);([^;]*).*");
    public static final DateTimeFormatter dif = DateTimeFormatter.ofPattern("HH:mm:ss");

    public static CSVTable parseFile(Path dir) throws IOException {
        BufferedReader bis = new BufferedReader(new FileReader(dir.toFile()));
        bis.readLine();
        String line;
        Matcher matcher;
        List<CSVRecord> records = new LinkedList<>();
        while ((line = bis.readLine()) != null) {
            matcher = pattern.matcher(line);
            if (matcher.find()) {
                records.add(CSVRecord.create(matcher.group(1), LocalTime.parse(matcher.group(2), dif),
                        Double.parseDouble(matcher.group(3))));
            }
        }

        return CSVTable.create(records, dir);
    }

    public static List<CSVTable> parseDir(Path dir) throws IOException {
        List<Path> files = Files.list(dir)
                .filter(e -> e.getFileName().toString().endsWith(".csv"))
                .collect(Collectors.toList());
        List<CSVTable> tables = new LinkedList<>();
        for (Path path : files) {
            tables.add(parseFile(path));
        }
        return tables;
    }
}