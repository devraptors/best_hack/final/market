package io.gitlab.devraptors;

import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class Start {
    public static void main(String[] args) throws IOException {
        List<CSVTable> tables = DataParser.parseDir(Path.of("/home/app/src/csvs"));
        int serverPort = 5001;
        HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);


        server.createContext("/api/tools", exchange -> {
            if ("GET".equals(exchange.getRequestMethod())) {

                JSONArray data = new JSONArray();
                for (int i = 0; i < tables.size(); i++){
                    Object table_name_obj = (tables.get(i).getRecords().get(0).getTicker());
                    data.add(table_name_obj);
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tools", data);

                System.out.println(jsonObject);

                OutputStream os = exchange.getResponseBody();
                System.out.println(os.toString());

                exchange.sendResponseHeaders(200, jsonObject.toString().getBytes().length);
                os.write(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
                os.flush();
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        });

        server.createContext("/api/tool/", exchange -> {
            if ("POST".equals(exchange.getRequestMethod())) {
                try {
                    InputStream stream = exchange.getRequestBody();
                    System.out.println("STREAM: "+stream);
                    JSONObject data = Start.parseJsonFromRequest(stream);
                    System.out.println("DATA: "+data);

                    JSONObject result = new JSONObject();
                    result.put("index", getIndexByName(data.get("tool").toString(), tables, Integer.parseInt(data.get("index").toString())));
                    result.put("time", tables.get(getIndexByName(data.get("tool").toString(), tables, Integer.parseInt(data.get("index").toString()))).getRecords().get(Integer.parseInt(data.get("index").toString())).getTime().toString());
                    result.put("last", tables.get(getIndexByName(data.get("tool").toString(), tables, Integer.parseInt(data.get("index").toString()))).getRecords().get(Integer.parseInt(data.get("index").toString())).getLast());
                    OutputStream os = exchange.getResponseBody();

                    System.out.println("RESULT: "+result);

                    exchange.sendResponseHeaders(200, result.toString().getBytes().length);
                    os.write(result.toString().getBytes(StandardCharsets.UTF_8));
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    System.out.println(e.toString());
                    exchange.sendResponseHeaders(500, -1);
                }
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        });

        server.start();
    }

    public static JSONObject parseJsonFromRequest(InputStream stream) throws IOException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject)jsonParser.parse(new InputStreamReader(stream, "UTF-8"));
        } catch (ParseException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }



    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();

        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split(name+"=")[1];
            map.put(name, value);
        }
        return map;
    }

    public static int getIndexByName(String name, List<CSVTable> tables, Integer index){
        for (int i = 0; i < tables.size(); i++){
            if(tables.get(i).getRecords().get(index).getTicker().equals(name)){
                return i;
            }
        }
        return -1;
    }

}
